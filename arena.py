#!/usr/bin/python3
"""
 -- Arena and Object classes (with derivates)

Handles the basic game physics simulation system

@author:      Mc128k
@copyright:   2014 myself
@contact:     self@mc128k.info

Few portions of this code belong to:
@author Michele Tomaiuolo - http://www.ce.unipr.it/people/tomamic
@license This software is free - http://www.gnu.org/licenses/gpl.html
"""


class Object:
    # States, what kind of object it is
    UNDEFINED = 0
    WALL = 1
    FLOOR = 3
    PLAYER = 5
    ENEMY = 7
    ENEMY_VULNERABLE = 8  # Enemy shot by bullet
    BULLET = 9
    BULLET_INACTIVE = 10

    def __init__(self, arena: 'Arena', x: int, y: int, w: int, h: int):
        self._x, self._y = x, y
        self._w, self._h = w, h
        self._arena = arena
        self._arena.add(self)
        self._inertia = 0
        self._state = self.UNDEFINED

    def hit(self, other: 'Object'):
        '''
        Handles collisions between two objects and makes corrections
        to the coordinates
        '''
        raise NotImplementedError('Abstract method')

    def rect(self) -> (int, int, int, int):
        '''
        Returns (left, top, width, height)
        '''
        return self._x, self._y, self._w, self._h

    def symbol(self) -> int:
        '''
        Return 0, or the index of current image
        '''
        raise NotImplementedError('Abstract method')

    def state(self) -> int:
        '''
        Collision type for the object, (for example wall, floor)
        as an integer, defined inside this superclass
        '''
        return self._state

    def remove(self):
        '''
        Removes the current object from the arena
        '''
        self._arena.remove(self)

    def visible(self):
        '''
        Does the object have to be visualized?
        '''
        return True


class Character(Object):
    _dx = 0  # The current x vector
    _jump_inertia = 30  # Inertial value for the jump action (-inertia -> 1 -> +inertia)
    _jump_inertia_cur = 0  # Jump inertia
    _jump_inertia_speed = 3
    _prev_y = 0  # Previous coordinate for calculation of inertia
    _prev2_y = 0  # Previous coordinate for calculation of inertia
    _omega = 0

    def __init__(self, arena: 'Arena', x: int, y: int, w: int, h: int):
        super().__init__(arena=arena, x=x, y=y, w=w, h=h)
        self._alive = True

    def move(self):
        if self._alive:
            # Save current coordinates
            self._prev2_y = self._prev_y
            self._prev_y = self._y

            # Jump mode
            if self._jump_inertia_cur < 0:
                self._omega = self._arena.omega(self._jump_inertia_cur)
                if -5 < self._jump_inertia_cur < 5:
                    self._jump_inertia_cur += int(self._jump_inertia_speed / 2)
                else:
                    self._jump_inertia_cur += self._jump_inertia_speed

            # Gravity
            else:
                self._omega = int(self._arena.omega(self._inertia))
                self._inertia += 1

            self._y += self._omega

            # Move x
            self._x += self._dx

            # Reset inertia if still
            if self._prev_y == self._prev2_y:
                self._inertia = 0

            # Check if out of bounds
            a_x, a_y, a_w, a_h = self._arena.rect()
            if self._x + self._w < a_x or self._x > a_x + a_w:
                self._alive = False
            if self._y + self._h < a_y or self._y > a_x + a_h:
                self._alive = False

    def hit(self, other: Object):
        o_x, o_y, o_w, o_h = other.rect()

        # One surface can collide in only one way
        if other.state() == self.WALL:
            # Collides from top, push up
            if o_y < self._y + self._w < o_y + self._omega + 1:
                self._y = o_y - self._w
                self._inertia = 0
            # Collides from bottom, push down
            elif o_y + o_h > self._y > o_y + o_h + self._omega - 1:
                if o_x - self._w < self._x < o_x + o_w:
                    self._y = o_y + o_h
                    self._inertia = 0
                    self._jump_inertia_cur = 0
            # Collides left, push right (x inside)
            elif o_x < self._x < o_x + o_w and o_y < self._y + self._h < o_y + o_h + self._h:
                self._x = o_x + o_w
            # Collides right, push left (xw inside)
            elif self._x + self._w > o_x and o_y < self._y + self._h < o_y + o_h + self._h:
                self._x = o_x - self._w

        elif other.state() == self.FLOOR:
            if o_y < self._y + self._w < o_y + self._omega + 1:
                self._y = o_y - self._w

    def jump(self):
        if self._jump_inertia_cur == 0 and self._inertia == 0:
            self._jump_inertia_cur = -self._jump_inertia
            self._inertia = 1

    def is_alive(self) -> bool:
        return self._alive

    def hurt(self):
        '''
        Callback when somebody else hurts this object
        '''
        raise NotImplementedError('Abstract method')


class Arena:
    def __init__(self, w: int, h: int):
        self._w, self._h = w, h
        self._objects = []
        self._gravity = 300

    def add(self, obj: 'Object'):
        self._objects.append(obj)

    def remove(self, obj: 'Object'):
        self._objects.remove(obj)

    def move_all(self):
        for obj in self.objects():
            obj.move()
            for other in self.objects():
                if self.check_collision(obj, other):
                    obj.hit(other)
                    other.hit(obj)

    def check_collision(self, c1: 'Object', c2: 'Object') -> bool:
        x1, y1, w1, h1 = c1.rect()
        x2, y2, w2, h2 = c2.rect()
        return (c1 is not c2
            and y2 < y1 + h1 and y1 < y2 + h2
            and x2 < x1 + w1 and x1 < x2 + w2)

    def objects(self) -> list:
        return list(self._objects)

    def omega(self, inertia: int):
        '''
        Returns the delta-y movement given the current inertia, as
        an integer. Can be positive or negative depending on the inertia
        '''
        if inertia < 0:
            return -int((self._gravity / 100) + (int(-inertia) ** (2 - 0.5))/8)
        else:
            return int((self._gravity / 100) + (int(inertia) ** (2 - 0.5))/8)

    def rect(self) -> (int, int, int, int):
        return (0, 0, self._w, self._h)

    def gravity(self, g=None) -> float:
        '''
        Setter/Getter for gravity
        '''
        if g is not None:
            self._gravity = float(g)
        return self._gravity
