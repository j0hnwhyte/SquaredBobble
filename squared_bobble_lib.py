#!/usr/bin/python3
"""
 -- Squared Bobble Game Elements

Handles the internal game routines, based on Arena
and Character classes

@author:      Mc128k
@copyright:   2014 myself
@contact:     self@mc128k.info

Few portions of this code belong to:
@author Michele Tomaiuolo - http://www.ce.unipr.it/people/tomamic
@license This software is free - http://www.gnu.org/licenses/gpl.html
"""

import pygame
import math
from arena import Character, Arena, Object
from random import randint


class Player(Character):
    bullet_reload_time = 5
    max_lives = 5
    max_grace_time = 40
    x_speed = 6  # Delta x for moving the player
    is_hurt = False

    def __init__(self, arena: Arena, x: int, y: int, player_id=1):
        super().__init__(arena=arena, x=x, y=y, w=30, h=30)
        self._state = self.PLAYER
        self.lives = self.max_lives
        self._bullet_reload = 0
        self._grace_time = 0
        self._direction = 1
        self.player_id = player_id

    def go_left(self):
        self._direction = -1
        self._dx = self._direction * self.x_speed

    def go_right(self):
        self._direction = +1
        self._dx = self._direction * self.x_speed

    def stay(self):
        self._dx = 0

    def move(self):
        super().move()

        # Reload bullets
        if self._bullet_reload > 0:
            self._bullet_reload -= 1

        # Reload grace time for hurt
        self.is_hurt = False
        if self._grace_time > 0:
            self._grace_time -= 1
            self.is_hurt = True

    def hit(self, other: Object):
        if other.state() == self.ENEMY:
            self.hurt()
        elif other.state() == self.ENEMY_VULNERABLE:
            other.hurt()
        elif other.state() == self.BULLET_INACTIVE:
            other.hurt()
        super().hit(other)

    def shoot(self):
        if self._bullet_reload <= 0:
            Bullet(self._arena, self._x, self._y, dx=self._direction)
            self._bullet_reload = self.bullet_reload_time

    def hurt(self):
        if self.lives == 0:
            self._alive = False
        elif self._grace_time <= 0:
            self.lives -= 1
            self._grace_time = self.max_grace_time


class Enemy(Character):
    # AI timers
    x_timer = 0  # Time before new x walk action
    x_timer_min = 8
    x_timer_max = 50
    j_timer = 0  # Time before new jump action
    j_timer_min = 18
    j_timer_max = 35
    respawn_timer = 250  # Respawn timer after being touched
    _respawn_timer_cur = respawn_timer

    y_speed_vulnerable = 6  # Speed when vulnerable, same as bullet
    x_speed = 4

    def __init__(self, arena: Arena, x: int, y: int):
        super().__init__(arena=arena, x=x, y=y, h=30, w=30)
        self._state = self.ENEMY
        self._hit = False  # Prevent double hits and removes
        self.enemy_id = randint(0, 1000)  # Identifier, used for different colors

    def move(self):
        # Enemy not hit while in bubble, respawn
        if self._respawn_timer_cur == 0:
            self._state = self.ENEMY
            self._hit = False
            self._respawn_timer_cur = self.respawn_timer

        if self._state == self.ENEMY_VULNERABLE:
            self._inertia += 1
            self._omega = -self._arena.omega(self._inertia)
            self._y += self._omega / self.y_speed_vulnerable
        else:
            if self.x_timer <= 0:
                self.x_timer = randint(self.x_timer_min,
                                       self.x_timer_max)
                if randint(0, 1) == 0:
                    self._dx = self.x_speed
                else:
                    self._dx = -self.x_speed
            if self.j_timer <= 0:
                self.jump()
                self.j_timer = randint(self.j_timer_min,
                                       self.j_timer_max)
            super().move()

        # Handle timers
        self.x_timer -= 1
        self.j_timer -= 1
        if self._state == self.ENEMY_VULNERABLE:
            self._respawn_timer_cur -= 1

    def hurt(self):
        if self._state == self.ENEMY_VULNERABLE and not self._hit:
            self._hit = True
            self.remove()
        elif self._state == self.ENEMY:
            self._state = self.ENEMY_VULNERABLE


class Bullet(Object):
    y_speed = 8  # Floating bullet speed
    _shoot_inertia_min = 30  # Change for longer shoots
    _shoot_inertia_max = 35  # Change for longer shoots
    _shoot_inertia_speed = 3

    def __init__(self, arena: 'Arena', x: int, y: int, dx: int):
        super().__init__(arena=arena, x=x, y=y, w=15, h=15)
        self._direction = dx
        self._hit = False  # Prevents double hits and removes
        self._state = self.BULLET
        self._shoot_inertia_cur = randint(self._shoot_inertia_min, self._shoot_inertia_max)

    def move(self):
        if self._shoot_inertia_cur > 0:
            self._omega = self._arena.omega(self._shoot_inertia_cur)
            self._x += self._omega * self._direction
            self._shoot_inertia_cur -= self._shoot_inertia_speed
        else:
            self._state = self.BULLET_INACTIVE
            self._omega = -self._arena.omega(self._inertia)
            self._y += self._omega / self.y_speed
            self._inertia += 1

        # Check if out of bounds, kill itself
        a_x, a_y, a_w, a_h = self._arena.rect()
        if self._x + self._w < a_x or self._x > a_x + a_w:
            self.remove()
        if self._y + self._h < a_y or self._y > a_x + a_h:
            self.remove()

    def hit(self, other: 'Object'):
        if self.state() == self.BULLET:  # Inactive can't hurt
            if other.state() == self.ENEMY:
                self._shoot_inertia_cur = 0
                other.hurt()
                self.hurt()
            elif other.state() == self.WALL:
                self._direction = 0
                self._shoot_inertia_cur = 0

    def hurt(self):
        if self._shoot_inertia_cur <= 0 and not self._hit:
            self._hit = True
            self.remove()


class Platform(Object):
    def __init__(self, arena: Arena, x: int, y: int, w: int,
                 h: int, state=-1, visible=True):
        super().__init__(arena=arena, x=x, y=y, w=w, h=h)
        self._visible = visible

        # Determine if it's a floor or a wall
        if state != -1: self._state = state
        else:
            if w > h: self._state = self.FLOOR
            else: self._state = self.WALL

    def move(self):
        pass

    def hit(self, other: Character):
        pass

    def visible(self):
        return self._visible


class Colors:
    def __init__(self, screen, clock):
        self._clock = clock
        self._screen = screen
        self._ck = pygame.time.Clock()

    def c(self, minimum, maximum, n, div, start=0) -> int:
        '''
        Configurable sinusoidal function for color waves (absolute value)
        Lisp hackers will love the parentheses orgy
        '''
        return int(minimum + ((maximum - minimum) * abs(math.sin((n / div) + start))))

    def c_half(self, minimum, maximum, n, div, start=0) -> int:
        '''
        Configurable sinusoidal function for color waves (positive part)
        '''
        return max(int(minimum + ((maximum - minimum) * abs(math.sin((n / div) + start)))), 0)

    def player(self, player_id: int, n: int, hurt=False):
        period = 30
        p0 = self.c_half(0, 255, n, period, 0)
        p1 = self.c_half(0, 255, n, period, math.pi * 2/3)
        p2 = self.c_half(0, 255, n, period, math.pi * 4/3)
        if player_id == 1:
            if hurt:
                return (p2, 0, 0)
            return (p2, p0, p1)
        elif player_id == 2:
            if hurt:
                return (p0, 0, 0)
            return (p0, p1, p2)

    def enemy(self, enemy_id: int, n: int, hurt=False):
        r = (math.pi / 4) * enemy_id
        m1 = math.sin(n / 100)
        e0 = self.c_half(120, 180 * m1, n, 11, start=r)
        e1 = self.c_half(0, 170, n, 12, start=r+20)
        e2 = self.c_half(30, 220, n, 10, start=r+30)
        e3 = self.c_half(30, 100, n, 2, start=r+30)
        if hurt: return (e1, e3, e1)
        return (e0, e1, e2)

    def walls(self, n: int):
        w0 = self.c_half(40, 140, n, 30, 18)
        w1 = self.c(80, 139, n, 70, 70)
        w2 = self.c_half(50, 210, n, 30, math.pi * 1.3)
        return (w0, w1, w2)

    def bullets(self, n: int, inactive: False):
        l0 = self.c(100, 240, n, 1, 0)
        l1 = self.c(100, 240, n, 2, 0)
        l2 = self.c(100, 240, n, 3, 0)
        l3 = self.c(180, 255, n, 10, 90)
        if inactive: return (200, 180, l3)
        return (l0, l1, l2)

    def background(self, n: int):
        b0 = self.c(220, 200, n, 50, 110)
        b1 = self.c_half(210, 235, n, 110, 23)
        b2 = self.c(180, 255, n, 90, 0)
        return (b0, b1, b2)

    def hud(self, n: int):
        c0 = self.c(90, 230, n, 30, start=3)
        c1 = self.c_half(90, 210, n, 70, start=2)
        c2 = self.c_half(90, 190, n, 40, start=math.pi/4)
        return (c0, c1, c2)

    def white_fade_in(self, t=80):
        white = pygame.Surface((640, 480), pygame.SRCALPHA)
        for i in range(0, t):
            alpha = ((155 / t) * i)
            white.fill((255, 255, 255, alpha))
            self._screen.blit(white, (0, 0))
            self._ck.tick(self._clock)
            pygame.display.flip()
        self._screen.fill((255, 255, 255))
        pygame.display.flip()

    def white_fade_out(self, arena, render, t=80):
        white = pygame.Surface((640, 480), pygame.SRCALPHA)
        for i in range(0, t):
            render.draw_frame(arena, 0)
            render.draw_hud(arena, 0)
            alpha = 255 -((255 / t) * i)
            white.fill((255, 255, 255, alpha))
            self._screen.blit(white, (0, 0))
            self._ck.tick(self._clock)
            pygame.display.flip()
        pygame.display.flip()


class Levels:
    '''
    Creates floors and walls inside the arena
    '''
    def __init__(self, arena):
        self._arena = arena

    def base(self):
        # Floor, walls
        Platform(self._arena, 0, 390, 640, 10)  # Bottom wall
        Platform(self._arena, 0, 0, 640, 10, state=Platform.WALL, visible=True)  # Upper wall
        Platform(self._arena, 0, 0, 10, 390, visible=True)  # Left wall
        Platform(self._arena, 630, 0, 10, 390, visible=True)  # Right wall

    def demo(self):
        # Walls
        Platform(self._arena, 0, 0, 640, 20, state=Platform.WALL, visible=False)
        Platform(self._arena, 0, 460, 640, 20, state=Platform.WALL, visible=False)
        Platform(self._arena, 0, 0, 20, 480, state=Platform.WALL, visible=False)
        Platform(self._arena, 620, 0, 20, 480, state=Platform.WALL, visible=False)

        # From top, to bottom
        Platform(self._arena, 100, 110, 150, 20)
        Platform(self._arena, 390, 110, 150, 20)
        Platform(self._arena, 0, 230, 140, 20)
        Platform(self._arena, 640 - 140, 230, 240, 20)

        # Large central platform
        Platform(self._arena, 100, 350, 150, 20)
        Platform(self._arena, 390, 350, 150, 20)

    def lvl_1(self):
        self.base()

        # Upper platforms
        Platform(self._arena, 130, 110, 180, 20)
        Platform(self._arena, 330, 110, 180, 20)

        # Lateral floors
        Platform(self._arena, 0, 200, 140, 20)
        Platform(self._arena, 640 - 140, 200, 240, 20)

        # Central platforms
        Platform(self._arena, 130, 300, 160, 20)
        Platform(self._arena, 350, 300, 160, 20)

        # Wall
        Platform(self._arena, 310, 110, 20, 120)


class Render:
    def __init__(self, screen, clock):
        self._screen = screen
        self._clock = clock
        self._ck = pygame.time.Clock()
        self._col = Colors(self._screen, self._clock)

    def draw_frame(self, arena, n):
        '''
        Draws a single frame on the screen, interprets
        objects from the arena and draws squares on the screen
        '''
        # Background
        self._screen.fill(self._col.background(n))

        # Draw enviromnent
        for c in arena.objects():
            s = c.state()

            if s == Object.WALL or s == Object.FLOOR and c.visible():
                col = self._col.walls(n)
                pygame.draw.rect(self._screen, col, c.rect())

        # Draw characters
        for c in arena.objects():
            s = c.state()
            draw = True

            if s == Object.PLAYER:
                if not c.is_alive(): draw = False
                col = self._col.player(c.player_id, n, c.is_hurt)

            elif s == Object.ENEMY:
                col = self._col.enemy(c.enemy_id, n, False)

            elif s == Object.ENEMY_VULNERABLE:
                col = self._col.enemy(c.enemy_id, n, True)

            elif s == Object.BULLET:
                col = self._col.bullets(n, False)

            elif s == Object.BULLET_INACTIVE:
                col = self._col.bullets(n, True)

            else:
                draw = False

            if c.visible() and draw:
                pygame.draw.rect(self._screen, col, c.rect())

    def intro(self):
        '''
        Introduction screen
        '''
        # Black
        self._screen.fill((0, 0, 0))
        pygame.display.flip()
        for n in range(0, 30):
            self._ck.tick(self._clock)

        # Black to white
        n = 0
        while n <= 255:
            self._screen.fill((n, n, n))
            pygame.display.flip()
            for e in pygame.event.get():
                if e.type == pygame.QUIT:
                    return -1
            self._ck.tick(self._clock)
            n += 4

        # Stay white, draw and reduce rectangle while changing color
        n = 0
        time = 500
        step = 5
        while n <= time:
            self._screen.fill((255, 255, 255))

            # Absolute timing, must end 0 to sync with menu
            w = (time / step) - (n / step)
            col = self._col.player(1, w, False)

            x = int(47 * math.log(1 + (120 / time) * n))
            y = int(30 * math.log(1 + (120 / time) * n))
            w = 640 - 2 * x
            h = 480 - 2 * y

            pygame.draw.rect(self._screen, col, (x, y, w, h))
            pygame.display.flip()
            self._ck.tick(self._clock)
            n += step
            for e in pygame.event.get():
                if e.type == pygame.QUIT:
                    return -1
                elif e.type == pygame.KEYDOWN:
                    if e.key == pygame.K_ESCAPE:
                        return -1

    def about(self):
        '''
        About, commands, info
        '''
        font = pygame.font.Font("./font.ttf", 24)
        text1 = font.render("There is no score in life", True, (170, 170, 170))
        text2 = font.render("You rise inside an arena", True, (170, 170, 170))
        text3 = font.render("It's hard to recognize yourself", True, (170, 170, 170))
        text4 = font.render("It's hard to recognize your enemies", True, (170, 170, 170))
        text5 = font.render("You strive to survive, you inevitably die", True, (170, 170, 170))
        text6 = font.render("P1 - Arrows           P2 -  WASD", True, (170, 170, 170))
        text7 = font.render("Design & Code:  2014 Mc128k", True, (170, 170, 170))

        text1_rect = text1.get_rect()
        text1_rect.center = (320, 80)
        text2_rect = text2.get_rect()
        text2_rect.center = (320, 122)
        text3_rect = text3.get_rect()
        text3_rect.center = (320, 164)
        text4_rect = text4.get_rect()
        text4_rect.center = (320, 206)
        text5_rect = text5.get_rect()
        text5_rect.center = (320, 248)
        text6_rect = text6.get_rect()
        text6_rect.center = (320, 390)
        text7_rect = text7.get_rect()
        text7_rect.center = (320, 420)

        white = pygame.Surface((640, 480), pygame.SRCALPHA)
        t = 100

        # Entry animation
        for i in range(0, t):
            alpha = 255 -((255 / t) * i)
            white.fill((255, 255, 255, alpha))
            self._screen.blit(text1, text1_rect)
            self._screen.blit(text2, text2_rect)
            self._screen.blit(text3, text3_rect)
            self._screen.blit(text4, text4_rect)
            self._screen.blit(text5, text5_rect)
            self._screen.blit(text6, text6_rect)
            self._screen.blit(text7, text7_rect)
            self._screen.blit(white, (0, 0))
            pygame.display.flip()
            self._ck.tick(self._clock)

        # Wait for user action
        while True:
            for e in pygame.event.get():
                if e.type == pygame.QUIT:
                    return -1
                elif e.type == pygame.KEYDOWN:
                    if e.key == pygame.K_ESCAPE:
                        return -1
                    elif e.key == pygame.K_SPACE or e.key == pygame.K_RETURN:
                        self._col.white_fade_in()
                        return 0

    def menu(self):
        '''
        Prints the main menu and returns the choice as an integer
        '''
        sandbox_arena = Arena(640, 480)
        levels = Levels(sandbox_arena)
        choice = 0
        n = 0

        # Place level
        levels.demo()

        # Place enemies, randomize
        for i in range(0, 15):
            Enemy(sandbox_arena, 320, 240)
        for i in range(0, 30):
            sandbox_arena.move_all()

        # The transparent overlay
        overlay = pygame.Surface((640, 480), pygame.SRCALPHA)

        # Menu
        menu_selector = pygame.Surface((100, 50), pygame.SRCALPHA)
        menu_new_1p = pygame.Surface((100, 50), pygame.SRCALPHA)
        menu_new_2p = pygame.Surface((100, 50), pygame.SRCALPHA)
        menu_about = pygame.Surface((100, 50), pygame.SRCALPHA)
        m_t = 0

        while True:
            # Sandbox arena in background
            sandbox_arena.move_all()
            self.draw_frame(sandbox_arena, n)

            # Colors for menu entries
            m_1 = self._col.c(150, 200, n, 69, start=3)
            m_2 = self._col.c(80, 121, n, 121, start=3)
            m_3 = self._col.c(130, 230, n, 37, start=5)
            m_4 = self._col.c(70, 130, n, 90, start=3)
            m_5 = self._col.c(150, 180, n, 55, start=8)

            # Overlay
            max_alpha = 160
            alpha = max((255 - n), max_alpha)
            overlay.fill((255, 255, 255, alpha))
            self._screen.blit(overlay, (0, 0))

            # Menu entries
            if n > 80 and m_t + 5 <= 205: m_t += 5
            menu_new_1p.fill((m_1, m_2, m_5, m_t))
            self._screen.blit(menu_new_1p, (170, 420))
            menu_new_2p.fill((m_3, m_5, m_2, m_t))
            self._screen.blit(menu_new_2p, (270, 420))
            menu_about.fill((m_4, m_3, m_1, m_t))
            self._screen.blit(menu_about, (370, 420))

            # Menu selector
            r = self._col.c(50, 240, n, 1)
            menu_selector.fill((r, 30, 30, m_t))
            if choice == 0: self._screen.blit(menu_selector, (170, 420))
            elif choice == 1: self._screen.blit(menu_selector, (270, 420))
            elif choice == 2: self._screen.blit(menu_selector, (370, 420))

            # Square at the center
            col = self._col.player(1, n, False)
            pygame.draw.rect(self._screen, col, (225, 143, 190, 194))
            pygame.display.flip()
            self._ck.tick(self._clock)
            n += 1

            # Handle events
            for e in pygame.event.get():
                if e.type == pygame.QUIT:
                    return -1
                elif e.type == pygame.KEYDOWN:
                    if e.key == pygame.K_ESCAPE:
                        return -1
                    elif e.key == pygame.K_SPACE or e.key == pygame.K_RETURN:
                        return choice
                    elif e.key == pygame.K_LEFT and choice > 0:
                        choice -= 1
                    elif e.key == pygame.K_RIGHT and choice < 2:
                            choice += 1

    def draw_hud(self, arena, n):
        '''
        Displays remaining lives in the bottom area
        '''
        pygame.draw.rect(self._screen, self._col.hud(n), (0, 400, 640, 80))

        # Lives
        p2_l = 0
        col2 = (0, 0, 0)
        for c in arena.objects():
            s = c.state()
            hurt = False
            if s == Object.PLAYER:
                if c.player_id == 2:
                    p2_l = c.lives
                    if c.is_hurt: hurt = True
                    col2 = self._col.player(2, n, hurt)
                else:
                    p1_l = c.lives
                    if c.is_hurt: hurt = True
                    col1 = self._col.player(1, n, hurt)
        for life in range(p1_l):
            pygame.draw.rect(self._screen, col1, (640 - 50 - 30 - 45 * life, 425, 30, 30))
        for life in range(p2_l):
            pygame.draw.rect(self._screen, col2, (50 + 45 * life, 425, 30, 30))
