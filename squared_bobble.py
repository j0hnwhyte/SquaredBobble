#!/usr/bin/python3
"""
 -- Squared Bobble Main game

Draws the game on the screen and handles the gameplay

@author:      Mc128k
@copyright:   2014 myself
@contact:     self@mc128k.info

Few portions of this code belong to:
@author Michele Tomaiuolo - http://www.ce.unipr.it/people/tomamic
@license This software is free - http://www.gnu.org/licenses/gpl.html
"""


import pygame
from arena import Arena
from squared_bobble_lib import Player, Platform, Enemy, Levels, Render, Colors
from random import randint

# Game speed
CLOCK = 40
FULLSCREEN = False

'''
Pygame Initialization
'''
pygame.init()
if FULLSCREEN:
    screen = pygame.display.set_mode((640, 480), pygame.FULLSCREEN)
    pygame.mouse.set_visible(False)
else:
    screen = pygame.display.set_mode((640, 480))
pygame.display.set_caption('Squared Bobble')
ck = pygame.time.Clock()
render = Render(screen, CLOCK)
col = Colors(screen, CLOCK)

'''
Game Intro
'''
if render.intro() == -1:
    col.white_fade_in()
    pygame.quit()
    exit()

'''
Menu
'''
while True:
    choice = render.menu()
    col.white_fade_in()
    if choice == -1:
        col.white_fade_in()
        pygame.quit()
        exit()
    elif choice == 0:
        players = 1
        break
    elif choice == 1:
        players = 2
        break
    elif choice == 2:
        if render.about() == -1:
            col.white_fade_in()
            pygame.quit()
            exit()

'''
Game Loop
'''
playing = True
while playing:
    if players == 2: p2 = True
    else: p2 = False

    arena = Arena(640, 480 - 80)
    levels = Levels(arena)
    difficulty_cur = 0

    # Regenerate players
    if p2: player2 = Player(arena, 20, 20 + 30, player_id=2)  # Upper left
    player1 = Player(arena, 640 - 20 - 30, 20 + 30)  # Upper right

    # Load levels
    levels.lvl_1()
    arena.move_all()
    col.white_fade_out(arena, render)

    player1_alive = True
    if p2: player2_alive = True
    else: player2_alive = False

    p1_l, p1_r, p2_l, p2_r = False, False, False, False
    n, spawn_timer = 0, 0
    while (player1_alive or player2_alive) and playing:
        # Handle events
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                playing = False

            # Players
            if e.type == pygame.KEYDOWN:
                if player1_alive:
                    if e.key == pygame.K_ESCAPE:
                        playing = False
                    elif e.key == pygame.K_UP:
                        player1.jump()
                    elif e.key == pygame.K_DOWN:
                        player1.shoot()
                    elif e.key == pygame.K_LEFT:
                        p1_l = True
                        p1_t = 'l'
                    elif e.key == pygame.K_RIGHT:
                        p1_r = True
                        p1_t = 'r'
                if p2 and player2_alive:
                    if e.key == pygame.K_w:
                        player2.jump()
                    elif e.key == pygame.K_s:
                        player2.shoot()
                    elif e.key == pygame.K_a:
                        p2_l = True
                        p2_t = 'l'
                    elif e.key == pygame.K_d:
                        p2_r = True
                        p2_t = 'r'
            if e.type == pygame.KEYUP:
                if e.key == pygame.K_LEFT:
                    p1_l = False
                elif e.key == pygame.K_RIGHT:
                    p1_r = False
                if p2:
                    if e.key == pygame.K_a:
                        p2_l = False
                    elif e.key == pygame.K_d:
                        p2_r = False

            # Calculate P1 movements
            if p1_l and not p1_r:
                player1.go_left()
            elif not p1_l and p1_r:
                player1.go_right()
            elif p1_l and p1_r:
                if p1_t == 'l':
                    player1.go_left()
                else:
                    player1.go_right()
            else:
                player1.stay()

            # Calculate P2 movements
            if p2:
                if p2_l and not p2_r:
                    player2.go_left()
                elif not p2_l and p2_r:
                    player2.go_right()
                elif p2_l and p2_r:
                    if p2_t == 'l':
                        player2.go_left()
                    else:
                        player2.go_right()
                else:
                    player2.stay()

        # Physics simulation step
        arena.move_all()

        # Draw stuff
        render.draw_frame(arena, n)
        render.draw_hud(arena, n)
        pygame.display.flip()

        # Check if alive
        player1_alive = player1.is_alive()
        if p2: player2_alive = player2.is_alive()

        # Timing routines
        ck.tick(CLOCK)
        spawn_timer -= 1
        n += 1

        # Spawn enemies
        if spawn_timer < 0:
            Enemy(arena, randint(250, 640 - 250), 60)
            spawn_timer = 250 + difficulty_cur
            if n % 700 and spawn_timer > 55:
                difficulty_cur -= 15
                if p2: difficulty_cur -= 8

    # If quitting or reloading, fade out
    col.white_fade_in()

pygame.quit()
exit()
